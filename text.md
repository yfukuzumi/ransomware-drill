# 目次
- [目次](#目次)
- [ハンズオン環境と概要](#ハンズオン環境と概要)
  - [ハンズオン環境](#ハンズオン環境)
  - [ハンズオンの概要](#ハンズオンの概要)
- [Cloud Insight Storage Workload Securityを利用したセキュリティインシデントへの対応](#cloud-insight-storage-workload-securityを利用したセキュリティインシデントへの対応)
  - [JUMP HOSTへリモートデスクトップ接続](#jump-hostへリモートデスクトップ接続)
  - [ランサムウェア攻撃のシミュレーション](#ランサムウェア攻撃のシミュレーション)
    - [Activityの確認](#activityの確認)
    - [疑似ランサムウェアスクリプトの実行](#疑似ランサムウェアスクリプトの実行)
    - [アラートの確認](#アラートの確認)
    - [アラートメールの確認](#アラートメールの確認)
  - [Snapshot/SnapRestore/FlexCloneを用いたランサムウェアからの復旧](#snapshotsnaprestoreflexcloneを用いたランサムウェアからの復旧)
    - [ユーザーご自身によるファイル単位のリカバリ](#ユーザーご自身によるファイル単位のリカバリ)
    - [簡単一発コース（初級）：管理者によるボリューム全体の瞬時のリカバリ](#簡単一発コース初級管理者によるボリューム全体の瞬時のリカバリ)
    - [リアルなクローン作成コース（中級)：FlexCloneによるクローンボリューム作成](#リアルなクローン作成コース中級flexcloneによるクローンボリューム作成)


<div style="page-break-before:always"></div>

# ハンズオン環境と概要
## ハンズオン環境
1. 本日のハンズオンは下記環境で行います
<img src="image/1-1.png" width="80%">  
<br>

【概要】
- クライアント端末：皆様が直接操作頂く物理PCです。
- Jump Host：クライアント端末からリモートデスクトップ接続するクラウド上の踏み台仮想マシンです。
- Cloud Volumes ONTAP：NetAppのクラウドファイルサーバーです。Jump HostはこのファイルサーバーをIドライブとして利用している想定です。(この中のファイルが感染するシナリオです。)
***
<div style="page-break-before:always"></div>

## ハンズオンの概要
本日は、NetAppのクラウドサービスによって、ファイルサーバーがランサムウェアへの感染を「検知」して即座に「通知」が可能な事や、スナップショットによる緊急での「保護」および迅速な「復旧」が可能であることを体感頂く内容となっています。

<img src="image/1-2.png" width="80%">  
<br>

<img src="image/1-3.png" width="80%">  
<br>


<div style="page-break-before:always"></div>


# Cloud Insight Storage Workload Securityを利用したセキュリティインシデントへの対応
## JUMP HOSTへリモートデスクトップ接続

**★実施する内容**  
ここでは受講者の皆様のクライアント端末から、Jump Hostへリモートデスクトップ接続を行います。  
<img src="image/2-1.png" width="80%">  
<br>

***

1. **各自のPC**から**Jump Host**へリモートデスクトップ接続を行う操作をします。「**Windows」ボタン**をクリックし、「**Windowsアクセサリ**」から「**リモートデスクトップ接続**」をクリックします。  
<img src="image/2-2.png" width="50%">  
<br>

***

2. 「**オプションの表示**」をクリックします。  
<img src="image/2-3.png" width="50%">  
<br>

***

3.	接続先として、**ご自身のJump HostのIPアドレス**と**ドメインユーザーの情報**を**受講者情報**の表より確認し、入力の上「**接続**」ボタンをクリックします。  
<img src="image/2-4.png" width="50%">  
<br>

***

4. 下記のメッセージが表示されたら、「**接続**」ボタンをクリックします。    
<img src="image/2-5.png" width="50%">  
<br>

***

5. 受講者情報を参照して、パスワードを入力して「**OK**」ボタンをクリックします。  
<img src="image/2-6.png" width="50%">  
<br>

***
6. 以下の情報が表示されたら、「**はい**」ボタンをクリックします。  
 <img src="image/2-9.png" width="50%">  
<br>

***
7. 	Windowsデスクトップが表示され、ログイン出来たことを確認します。  
<img src="image/2-10.png" width="50%">  
<br>


<div style="page-break-before:always"></div>


## ランサムウェア攻撃のシミュレーション
★実施する内容  

ここではランサムウェアに感染し、ファイルサーバーのデータが暗号化されてしまうことを疑似体験します。簡易的なスクリプトを動作させ、テキストファイルが暗号化され拡張子が.lolに変わってしまう事を確認します。

<img src="image/2-11.png" width="85%">  
<br>

***

1. Windowsのスタートボタンの横にあるショートカットをクリックして、**エクスプローラー**を開きます。  
<img src="image/2-12.png" width="50%">  
<br>

***

2. 「**This PC**」をクリックし、「**Network locations**」に表示されている**Iドライブ**をダブルクリックします。  

<img src="image/2-13.png" width="60%">  
<br>

***

3. 	**Iドライブ**の中に5,000個のテスト用テキストファイルがある事を確認します。適当にファイルを開いてください。  

<img src="image/2-14.png" width="60%">  
<br>

<div style="page-break-before:always"></div>

### Activityの確認
★実施する内容  

ここではNetAppのクラウドサービスがユーザーの行動をどのように検知しているのか、確認していきます。

<img src="image/2-15.png" width="80%">  
<br>

<img src="image/2-16.png" width="80%">  
<br>

***

1. https://hg3197.c01-ap-1.cloudinsights.netapp.com/web/ へWebブラウザから接続します。  
2. 下記ユーザー情報を入力して「**LOG IN**」をクリックします。  
ユーザー名：**networld-cshandson@outlook.jp**  
パスワード：**Networld2020**  
<img src="image/2-17.png" width="80%">  
<br>

***

3. 左カラムからWorkload Security＞Forensics＞**Activity Forensics**を選択します。
<img src="image/2-18.png" width="80%">  
<br>

***

4. All Acitivityに先程ご自身がファイルサーバーへ実施した操作（該当ファイル／操作／時間）が**記録されている**ことを確認します。他参加者のActivityも記録されているため、分かりづらい場合はFilterを追加して「**User**」でフィルターをかけてください。 ご自身のActivityがほぼリアルタイムに反映されることを確認してください。
<img src="image/2-19.png" width="80%">  
<br>

<div style="page-break-before:always"></div>

### 疑似ランサムウェアスクリプトの実行
1. 感染ホストのデスクトップ上にある、「**Simulate_Rename_Files**」を右クリックして「**PowerShellで実行**」をクリックします。  
<img src="image/2-20.png" width="50%">  
<br>

***
2. 	PowerShellウィンドウが表示されます。「**Simulating ransomware**」が表示されたら、そのまましばらく放置します。  
<img src="image/2-21.png" width="60%">  
<br>

***
3. 	ときどきエクスプローラーの更新ボタンを押して**Iドライブ**に作成していたテキストファイルの拡張子が「**.txt.lol**」にどんどん変わっていくことを確認します。  
<img src="image/2-22.png" width="80%">  
<br>

***
4. 	暗号化されたファイルを開いてみてください。ファイルの中身が参照できるかどうか確認します。  

5. 	少し立つと途中で暗号化がエラーになります。これはCloud Insight Storage Workload Securityの「**Automation Response Policy**」が、ご自身を攻撃者と判定し、ストレージへのアクセスをブロックしたためです。ファイルサーバーにアクセスできなくなっていることを確認して、PowerShellのウィンドウを閉じます。
<img src="image/2-23.png" width="80%">  
<br>

***
6. 	続いて、このランサムウェア攻撃をNetAppのサービスでどのように検知しているのかを次章から確認していきます。  

<div style="page-break-before:always"></div>

### アラートの確認
**★実施する内容**  
ここではランサムウェアによるファイル暗号化をNetAppのクラウドサービスがどのように検知しているのか、確認していきます。

1. **Cloud Insight Storage Workload Security**の管理画面左にある「**ALERTS**」を選択します。  
<img src="image/2-24.png" width="80%">  
<br>

***
2. 	**ご自身のユーザー名**で**Ransomware Attack** アラートが上がっていることを確認します。  
<img src="image/2-25.png" width="80%">  
<br>

***
3. 	アラートの一番左の項目の「**アラートID**」をクリックします。  

4. **Automation Response Policy**によって、自動的にスナップショットが取得されたことを確認します。
また、「**Snapshots Taken**」と表示されていることも確認します。  
<img src="image/2-26.png" width="80%">  
<br>

***
5.  次はどのユーザーがブロックされたのかを確認するために「**user account**」のリンクをクリックします。  
<img src="image/2-27.png" width="80%">  
<br>

***

6.	アラートにあがったユーザー名が表示され、**User/IP Access**が**Blocked**になっていることを確認します。  
<img src="image/2-28.png" width="60%">  
<br>

***

7. **ユーザー名**をクリックします。  
<img src="image/2-29.png" width="50%">  
<br>

***

8. ブロックされたユーザーに対して、ブロック期間の変更、ブロック解除といった制御操作が出来ます。※まだこの段階では操作しないでください。  
<img src="image/2-30.png" width="50%">  
<br>

<div style="page-break-before:always"></div>

### アラートメールの確認
ここではNetAppのクラウドサービスから、管理者様のメールアドレスにメール通知が行われたかどうかを確認します。  
<img src="image/2-31.png" width="60%">  
<br>

***
1.	アラートがメールでも通知されている事を確認するためにブラウザを起動して以下のURLにアクセスして、画面右上にある「**Sign in**」ボタンをクリックします。  
https://outlook.live.com/owa/  
<img src="image/2-32.png" width="60%">  
<br>

***
2. ここは、受講者様共通で「**networld-cshandson@outlook.jp**」を入力し「**Next**」ボタンをクリックします。  
<img src="image/2-33.png" width="50%">  
<br>

***

3. パスワード「**Networld2020**」を入力し「**サインイン/Signin**」ボタンをクリックします。  
<img src="image/2-34.png" width="50%">  
<br>

***
4. 下記の画面が表示された場合は「**はい/Yes**」ボタンをクリックします。  
<img src="image/2-35.png" width="50%">  
<br>

***
5. 受信トレイに「**Critical Alert: Ransomware attack from user ‘csXXXXXX-XX’ account**」といったタイトルのメールが届いていることを確認します。 

<img src="image/2-36.png" width="60%">  
<br>

<div style="page-break-before:always"></div>


## Snapshot/SnapRestore/FlexCloneを用いたランサムウェアからの復旧
### ユーザーご自身によるファイル単位のリカバリ
ここでは**NetAppの機能で自動的に作成されたSnapshot**から、ユーザー様自身の手でファイル単位での復旧をご体験頂きます。  
<img src="image/3-1.png" width="80%">  
<br>

*** 
1. ここまで操作していた感染用ホストは、AD Userにてアクセスがブロックされた状態となっている為、まずはCloud Insight Storage Workload Securityを操作して**UserのBlockを解除**します。（実際にランサムウェアの被害にあった場合は、Client PCの安全性が確認されてから実施いただくことが前提になります。管理端末から操作頂くイメージです)


2. Cloud Insight Storage Workload SecurityのUser管理画面で「**Unblock User**」をクリックします。  
<img src="image/3-2.png" width="60%">  
<br>

*** 
3. ポップアップ画面で改めて「**Unblock User**」をクリックします。  
<img src="image/3-3.png" width="60%">  
<br>

*** 
4. Blockが解除され、**Iドライブにアクセスできる**ことを確認します。  
<img src="image/3-4.png" width="60%">  
<br>
<img src="image/3-5.png" width="60%">  
<br>

*** 


5. **Iドライブ**をダ**ブルクリック**して、アドレスバーに**I:¥~snapshot**と入力してIドライブの「 **~snapshot**」フォルダを開きます。  
表示されたフォルダから、「**cloudsecure_attack_auto_xxxxxx…**」フォルダを開きます。  
<img src="image/3-6.png" width="60%">  
<br>

*** 

6. 表示されたファイルは、ランサムウェア検知時に取得されたスナップショットで保護されたファイル群です。リストアしたいファイルを選択して、ドラッグアンドドロップでコピーしてリストアが可能な状態です。ここでは、**任意の.txt**ファイルをデスクトップや、任意のフォルダにコピーしてみます。※元のファイルサーバーには戻さないでください。  
<img src="image/3-7.png" width="80%">  
<br>


※Snapshot取得直後に感染UserのCIFSアクセスをブロックしているため、自動取得されたSnapshotで保護できたファイルは少数になっています。  

＜ご参考＞
Iドライブを右クリックして「**プロパティ**」を開き、「**以前のバージョン**」からも、取得されているSnapshotの世代が確認できますので、任意の世代のSnapshotからファイル単位で自由にリストア頂けます。  

<img src="image/3-8.png" width="50%">  
<br>


<div style="page-break-before:always"></div>

### 簡単一発コース（初級）：管理者によるボリューム全体の瞬時のリカバリ
>「**簡単一発コース（初級）**」を選択した場合、次の「**リアルなクローン作成コース（中級）**」は実施できません。どちらかを実施してください。  

ここではNetAppの機能で**自動的に作成されたSnapshot**から、管理者様が一括でリカバリする事を想定して、コマンドを使ってボリューム全体が瞬時にリカバリできる事をご体験頂きます。  
※以下のように元のテキストデータに戻るところが確認出来ます。

<img src="image/3-9.png" width="80%">  
<br>

*** 

1. ONTAPの管理画面にアクセスするため、**Chromeをダブルクリック**して立ち上げます。  
<img src="image/3-10.png" width="30%">  
<br>

*** 
2. URLバーに別紙記載の**ONTAP管理画面IPアドレス**を入力します。  
https://192.168.X.X　と必ずhttpsを明示的に入力します。  

3. 下記画面が表示されたら「**詳細設定**」をクリックし、「**192.168.X.Xにアクセスする（安全ではありません）**」をクリックします。  
<img src="image/3-11.png" width="40%">  
<br>

*** 

4. 下記画面が表示されたら「**ONTAP System Managerに直接サインインします**」をクリックします。  
<img src="image/3-12.png" width="40%">  
<br>

*** 

5. ユーザー名：**admin**  
 パスワード：**Networld2020**  
 と入力し「**サインイン**」をクリックします。  
 <img src="image/3-13.png" width="40%">  
<br>

*** 
6. ONTAPの管理画面にログインできました。  
<img src="image/3-14.png" width="80%">  
<br>

*** 
7. **ストレージ**＞**ボリューム**を選択し、**別紙記載の自身のボリューム名**をクリックします。  
<img src="image/3-15.png" width="80%">  
<br>

*** 
8. **Snapshotコピー**タブを選択し、**CloudSecure_attack_auto_XXXXXXX**という名前のSnapshotを探します。︙をクリックし「**リストア**」を選択します。別紙記載の自身のボリューム名をクリックします。  
<img src="image/3-16.png" width="80%">  
<br>

*** 

9.	「**このSnapshotコピーからボリュームをリストア**」にチェックを入れ、「**リストア**」をクリックします。  
<img src="image/3-17.png" width="80%">  
<br>

***

10. ボリュームがリカバリされたかどうかを確認する為に、エクスプローラーから**Iドライブ**を開きます。開きっぱなしだった場合は更新ボタンを押します。フォルダの状態はCloud Insight Storage Workload Securityが攻撃を検知した段階に戻っていることを確認します。ただし、この段階では攻撃の初期に暗号化されてしまったファイルは復元できていません。攻撃を検知後にSnapshotを自動取得しているため、検知前に暗号化された一部のデータは復元できないのです。  
<img src="image/3-18.png" width="80%">  
<br>

***

11. そこでスケジュール取得されたSnapshotをリストアしてボリュームを攻撃前の状態に戻します。自動取得された**CloudSecure_attack_auto_XXXXXXX**というSnapshotのひとつ前のSnapshot（Hourly.XXXX-XX-XX_XXXX）を選択し、同じ手順で「**リストア**」します。  
<img src="image/3-19.png" width="80%">  
<br>

***

12. ボリューム全体がリカバリされたかどうかを確認する為に、エクスプローラーから**Iドライブ**を開いて、すべてのファイル群の拡張子が「**.txt.lol**」ではなく、「**.txt**」に戻っている事を確認します。

<img src="image/3-20.png" width="80%">  
<br>

以上

<div style="page-break-before:always"></div>

### リアルなクローン作成コース（中級)：FlexCloneによるクローンボリューム作成
>「**簡単一発コース（初級）**」を実施した場合、以下手順は実施できません。  

以下の手順では、元ボリュームからクローンボリュームを作成します。クローンボリューム側で業務再開し、元ボリュームは被害状況調査のため、現状維持とします。  
<img src="image/3-21.png" width="80%">  
<br>

***
1. ONTAPの管理画面にアクセスするため、**Chromeをダブルクリック**して立ち上げます。  
<img src="image/3-10.png" width="30%">  
<br>

*** 
2. URLバーに別紙記載の**ONTAP管理画面IPアドレス**を入力します。  
https://192.168.X.X　と必ずhttpsを明示的に入力します。  

3. 下記画面が表示されたら「**詳細設定**」をクリックし、「**192.168.X.Xにアクセスする（安全ではありません）**」をクリックします。  
<img src="image/3-11.png" width="30%">  
<br>

*** 

4. 下記画面が表示されたら「**ONTAP System Managerに直接サインインします**」をクリックします。  
<img src="image/3-12.png" width="40%">  
<br>

*** 

5. ユーザー名：**admin**  
 パスワード：**Networld2020**  
 と入力し「**サインイン**」をクリックします。  
 <img src="image/3-13.png" width="40%">  
<br>

*** 
6. ONTAPの管理画面にログインできました。  
<img src="image/3-14.png" width="80%">  
<br>

*** 
7. **ストレージ**＞**ボリューム**を選択し、**別紙記載の自身のボリューム名**をクリックします。  
<img src="image/3-15.png" width="80%">  
<br>

*** 
8. 「**Snapshotコピー**」タブを選択し、**CloudSecure_attack_auto_XXXXXXX**という名前のSnapshotを探します。︙をクリックし「**クローンボリューム**」を選択します。
<img src="image/3-22.png" width="80%">  
<br>

*** 

9.  名前はデフォルトのまま（忘れないようメモしてください）、「シンプロビジョニング」にチェックが入っていることを確認して、「**クローン**」をクリックします。  
<img src="image/3-23.png" width="50%">  
<br>

*** 

10. しばらく待つとボリューム一覧に先程作成したクローンボリュームが表示されますので、**ボリューム名**をクリックします。  
<img src="image/tam17.png" width="80%">  
<br>

*** 

11. 次に「**編集**」をクリックします。  
<img src="image/3-35.png" width="80%">  
<br>

*** 

12.  ボリュームの編集画面に遷移しますので、エクスポート設定欄の「**マウント**」にチェックを入れ、「**保存**」をクリックします。※パスはデフォルトでOKです

<img src="image/3-25.png" width="80%">  
<br>

*** 

13. 次に、System Managerからクローンボリュームの共有を作成します。**ストレージ**＞**共有**から「**＋追加**」をクリックし、「**共有**」を選択します。  
<img src="image/3-26.png" width="80%">  
<br>

*** 

14. 共有名に「**cshandson_volXX_clone**」（利用している共有名＋Clone）と入力し、「**参照**」をクリックします。  
<img src="image/3-26-1.png" width="80%">  
<br>

*** 

15.  先程作成した**ご自身のクローンボリューム**を選択して、「**保存**」をクリックします。  
<img src="image/3-27.png" width="40%">  
<br>

*** 

16. アクセス権限の「**+追加**」をクリックします。  
<img src="image/3-28.png" width="80%">  
<br>

*** 

17. 以下のように入力されていることを確認します。  
ユーザ/グループ：**Everyone**  
ユーザタイプ：**Windows**  
アクセス権限：**フルコントロール**  
<img src="image/3-29.png" width="60%">  
<br>

*** 

18.  「**保存**」をクリックします。  
<img src="image/3-30.png" width="80%">  
<br>

*** 

19. エクスプローラーで**¥¥CVO1**にアクセスします。(¥¥しーぶいおー1)  
    **ご自身が作成した共有**(クローン)をクリックします。  
<img src="image/3-31.png" width="80%">  
<br>

*** 

1.   **暗号化されてしまったファイル**（拡張子が.lolになっているファイル）をすべて選択し、右クリックから「**削除**」します。  
<img src="image/3-32.png" width="80%">  
<br>

*** 

21. エクスプローラーから**Iドライブ**を右クリックして、**以前のバージョン**を選択します。**暗号化される直前に取得されたスケジュールSnapshot**を選択して、「**開く**」をクリックします。  
<img src="image/3-33.png" width="50%">  
<br>

*** 

22. 以前のバージョンで開いたエクスプローラー（暗号化される直前のSnapshotフォルダ）から、**先程削除したファイルと同じ番号のテキストファイル**を選択して、クローンボリュームの共有へドラッグアンドドロップして**コピー**します。  
<img src="image/3-34.png" width="80%">  
<br>


これでクローンボリュームの共有は限りなく損失の少ない状態で復元できました。  
暗号化されたファイル：直前のSnapshotから復元、最大でも一時間前の状態で復元  
暗号化されなかったファイル：攻撃を受けた時点の状態で復元

攻撃を受けたボリューム/共有は被害状況の確認のため（どのファイルが暗号されたか調査するため）、しばらく現状を保持しておきます。必要に応じてアクセス権限を変更し、読み取り専用にしたりアクセスを禁止させます。　　
***
23. 仕上げとしてクローンボリュームを攻撃を受けた元ボリュームと完全に切り離します。ストレージ＞ボリュームからクローンボリュームを選択し、**詳細**＞**クローン**のスプリットをクリックします。。
    
<img src="image/3-36.png" width="80%">  
<br>

***
24. 「**Snapshotコピーの削除とクローンのスプリット**」にチェックを入れて、「**スプリット**」をクリック します。しばらく待つとスプリットが完了した旨が表示されます。

<img src="image/3-37.png" width="50%">  
<br>

これで元ボリュームと復旧ボリュームは完全に切り離されました。

以上

