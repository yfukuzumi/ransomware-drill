# 消せないSnapshot：Tamperproof Snapshot
## Tamperproof Snapshotの設定
これまでお試しいただいたNetAppのSnapshot/SnapRestoreによるデータ復旧、Cloud Insight Storage Workload Security によるランサムウェア攻撃の早期検知により、ランサムウェアによる被害はほぼ防げるはずです。ただし、万全ではありません。万一NetAppの管理者権限を攻撃者に奪われた場合、Snapshotを消された上で暗号化されてしまう可能性が残ります。そのような管理者権限搾取に対抗する手段の一つとして、ここでは**管理者でも消せないSnapshot**,**Tamperproof Snapshot**を試します。
<img src="image/tam1.png" width="80%">  
<br>

1. ONTAP System Managerで**ストレージ**＞**ボリューム**＞**ご自身のボリューム**を選び、右側の設定ボタン︙ をクリックします。**編集**＞**ボリューム**をクリックします。  
<img src="image/tam2.png" width="60%">  
<br><span class='totalPages'></span>

***

2. 「**Snapshotロックを有効にする**」にチェックを入れます。

<img src="image/tam3.png" width="60%">  
<br>

***

3. 「**保存**」をクリックします。  
<img src="image/tam4.png" width="60%">  
<br>

<div style="page-break-before:always"></div>

## SnapLockコンプライアンスクロックの確認
1. **クラスタ**＞**概要**で**現在の時刻**を確認します。  
<img src="image/tam5.png" width="80%">  
<br>

***

2. ノード欄の「**表示/非表示**」をクリックします。  
<img src="image/tam6.png" width="80%">  
<br>

***

3. 「**SnapLockコンプライアンスクロック**」にチェックを入れます。  
<img src="image/tam7.png" width="80%">  
<br>

> **コンプライアンスクロックとは**  
> SnapLockはコンプライアンスクロックを基準にWORMファイルの保持期間の変更を防止します。コンプライアンスクロックを初期化すると、再度初期化することはできません。このハンズオンではすでに初期化されています。  



***

4. 「**SnapLockコンプライアンスクロック**」の時間と日時を比較します。多少ズレいてる可能性がありますが、このあとの手順では**SnapLockコンプライアンスクロック**の時間を基準にハンズオンを進めていきます。※SnapLockコンプライアンスクロックが画面に隠れている場合はバーを左側に移動して調整してください  
<img src="image/tam8.png" width="80%">  
<br>


> **SnapLockコンプライアンスクロックはなぜシステム時間とズレるのか？**  
> SnapLockコンプライアンスクロックは一度初期化すると原則一切変更することはできません。オンプレミスのFASシリーズなどをデータセンターではなくオフィス等に設置している場合、法定停電などでクラスターを停止させる可能性があります。クラスターを再起動させると、SnapLockコンプライアンスクロックは停止時間を無視して時刻を再開させます。一方、システム時間は設定したNTPと同期するため正確な時間を刻みます。SnapLockコンプライアンスクロックはNTPと同期することはできません。攻撃者がNTPやシステム時間を不正に操作してWORMを解除してしまう事態を防ぐためと思われます。Cloud Volumes ONTAPやONTAP Selectについてはクラスター停止を前提としているため、コンプライアンスクロックをNTPに同期させる仕組みがあります。SnapLockコンプライアンスクロックとNTPの時刻が一日以上ズレた場合は、コンプライアンスクロックを補正してくれます。詳しくは[マニュアル](https://docs.netapp.com/ja-jp/ontap/snaplock/initialize-complianceclock-task.html#ntp%E3%81%8C%E8%A8%AD%E5%AE%9A%E3%81%95%E3%82%8C%E3%81%9F%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0%E3%81%AB%E5%AF%BE%E3%81%97%E3%81%A6complianceclock%E3%81%AE%E5%86%8D%E5%90%8C%E6%9C%9F%E3%82%92%E6%9C%89%E5%8A%B9%E3%81%AB%E3%81%97%E3%81%BE%E3%81%99)をご参照ください。  

<div style="page-break-before:always"></div>

## Tamperproof Snapshotの取得
1. 改めて**ストレージ**＞**ボリューム**＞**ご自身のボリューム名**をクリックします。  
<img src="image/tam9.png" width="80%">  
<br>

***

2. 「**Snapshotコピータブ**」を選択して、「**＋追加**」をクリックします。  
<img src="image/tam10.png" width="80%">  
<br>

***

3. これまでは表示されていなかった「**SNAPLOCKの有効機期限（オプション）**」という欄が出現しています。**カレンダーマーク**をクリックします。  
<img src="image/tam11.png" width="60%">  
<br>

***

4. 先程確認したSnapLockコンプライアンスクロックを改めて確認してください。SnapLockコンプライアンスクロックの**5分後の日時**を入力します。時間は24時間表記で入力すると自動的に12時間表記に変更されます。「**追加**」をクリックします。これで、管理者でも管理者権限を奪取した攻撃者でも、5分間は削除できないSnapshotを作成することができました。  
<img src="image/tam12.png" width="80%">  
<br>

***

5. GUIにSnapLockの有効期限を表示させます。「**表示／非表示**」欄をクリックして、「**SnapLockの有効期限**」にチェック入れます。  
<img src="image/tam13.png" width="80%">  
<br>

***

6. Tamperproof Snapshotが本当に管理者でも削除できないか確認します。**先程取得したSnapshot**にチェック入れて、「**削除**」をクリックします。  
<img src="image/tam14.png" width="80%">  
<br>

***

7. 「**Snapshotコピーの削除**」にチェック入れて、「**削除**」をクリックします。  
<img src="image/tam15.png" width="60%">  
<br>

***

8. エラーメッセージが表示され、**削除に失敗した**ことを確認します。  
    <img src="image/tam16.png" width="80%">  
<br>

***

9. 同様の手順で、スケジュールで取得されたSnapshot（有効期限を指定していない）を**削除できるか試してください**。  
    ※有効期限を指定していないSnapshotでも「Snapshotの有効期限」は表示されます。過去の日時になっているはずです。

10.  5分経過後、改めて同様の手順で**Tamerproof Snapshotを削除できるか**確認してください。

以上